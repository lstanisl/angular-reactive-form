import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function samePasswordValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const isSame = control.value.password === control.value.confirmPassword;

    return !isSame ? { samePassword: { value: control.value } } : null;
  };
}
